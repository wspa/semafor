package com.semafor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.NoSuchElementException;

public class Semafor {

    public final String FILE_NAME = "semafor_config.txt";

    public final int WEST_SIDE = 1;
    public final int EAST_SIDE = 2;

    public final int CAR_TIME = 200;
    public final int SEMAFOR_TIME = 1000;

    private JPanel mainPanel;
    private JButton jBtnAdd10VehiclesToWest;
    private JButton jBtnAdd10VehiclesToEast;
    private JButton jBtnAddVehiclesToWest;
    private JButton jBtnAddVehiclesToEast;
    private JSpinner jSpinnerEast;
    private JSpinner jSpinnerWest;
    private JPanel jPanelWestSemafor;
    private JPanel jPanelEastSemafor;
    private JLabel jLblEastVehicleCount;
    private JLabel jLblWestVehicleCount;

    private boolean initiated = false;

    private int getSpinnerValueAsInteger(JSpinner spinner) {
        Number spinnerValue = (Number) spinner.getValue();
        return spinnerValue.intValue();
    }

    private void redrawCounters() {
        jLblWestVehicleCount.setText(Integer.toString(westThread.getVehicles()));
        jLblEastVehicleCount.setText(Integer.toString(eastThread.getVehicles()));
    }

    private void addVehicles(int count, int side) {
        if (side == WEST_SIDE) {
            westThread.addVehicles(count);
            redrawCounters();
        } else if (side == EAST_SIDE) {
            eastThread.addVehicles(count);
            redrawCounters();
        }
    }

    private void init() {
        if (!initiated) {
            loadState();
            westThread.start();
            eastThread.start();
            bridgeThread.start();
            initiated = true;
        }
    }

    private void loadState() {
        try {
            File file = new File(FILE_NAME);
            Scanner in = new Scanner(file);
            eastThread.setVehicles(Integer.parseInt(in.nextLine()));
            westThread.setVehicles(Integer.parseInt(in.nextLine()));
            bridgeThread.setSemaforState(Integer.parseInt(in.nextLine()));
        } catch (FileNotFoundException ex) {
            //ToDo exception handling :)
        } catch (NoSuchElementException ex) {
            //ToDo exception handling :)
        }
    }

    private void saveState() {
        if (initiated) {
            try {
                PrintWriter file = new PrintWriter(FILE_NAME);
                file.println(eastThread.getVehicles());
                file.println(westThread.getVehicles());
                file.println(bridgeThread.getSemaforState());
                file.close();
            } catch (FileNotFoundException ex) {
                //ToDo exception handling :)
            }
        }
    }

    public Semafor() {
        init();
        jBtnAddVehiclesToWest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addVehicles(getSpinnerValueAsInteger(jSpinnerWest), WEST_SIDE);
            }
        });
        jBtnAddVehiclesToEast.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addVehicles(getSpinnerValueAsInteger(jSpinnerEast), EAST_SIDE);
            }
        });
        jBtnAdd10VehiclesToEast.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addVehicles(10, EAST_SIDE);
            }
        });
        jBtnAdd10VehiclesToWest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addVehicles(10, WEST_SIDE);
            }
        });
    }

    class BridgeThread extends Thread {
        private int semaforState = EAST_SIDE;

        public int getSemaforState() {
            return semaforState;
        }

        public void setSemaforState(int semaforState) {
            this.semaforState = semaforState;
        }

        private void toggleSemafor() {
            if (semaforState == WEST_SIDE) {
                semaforState = EAST_SIDE;
                jPanelEastSemafor.setBackground(Color.GREEN);
                jPanelWestSemafor.setBackground(Color.RED);
            } else if (semaforState == EAST_SIDE) {
                semaforState = WEST_SIDE;
                jPanelWestSemafor.setBackground(Color.GREEN);
                jPanelEastSemafor.setBackground(Color.RED);
            }
        }

        public void run() {
            while (true) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        toggleSemafor();
                        saveState();
                    }
                });
                try {
                    Thread.sleep(SEMAFOR_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class SideThread extends Thread {
        private int vehicleCounter = 0;
        private int side;

        public void addVehicles(int count) {
            vehicleCounter += count;
        }

        public int getVehicles() {
            return vehicleCounter;
        }

        public void setVehicles(int vehicleCounter){
            this.vehicleCounter = vehicleCounter;
        }

        private void passVehicle() {
            if (vehicleCounter > 0) {
                vehicleCounter--;
                redrawCounters();
            }
        }

        SideThread(int side) {
            this.side = side;
        }

        public void run() {
            while (true) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        if (bridgeThread.getSemaforState() == side) {
                            passVehicle();
                        }
                    }
                });
                try {
                    Thread.sleep(CAR_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private SideThread eastThread = new SideThread(EAST_SIDE);
    private SideThread westThread = new SideThread(WEST_SIDE);
    private BridgeThread bridgeThread = new BridgeThread();

    public static void main(String[] args) {
        JFrame frame = new JFrame("Semafor");
        frame.setContentPane(new Semafor().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
